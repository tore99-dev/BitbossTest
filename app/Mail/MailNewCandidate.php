<?php

namespace App\Mail;

use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Auth;

class MailNewCandidate extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(User $user)
    {
        $this->user = $user;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build(User $user)
    {
        $user = $this->user;
        $userid = $user->id;
        $candidato = User::join('applications','users.id','=','applications.user_id')
            ->where('users.id',Auth::user()->id)
            ->select('users.id as iduser','users.email as emailuser',
                'applications.*','applications.id as idapp','applications.email as emailapp')
            ->first();
        return $this->markdown('mails.newcandidate')
            ->subject('Nuova candidatura da '.$user->name)
            ->with(['candidato'=>$candidato]);
    }
}
