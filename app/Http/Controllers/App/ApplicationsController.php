<?php

namespace App\Http\Controllers\App;

use App\Http\Controllers\Controller;
use App\Mail\MailApproveApplication;
use App\Mail\MailNewCandidate;
use App\Models\Application;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;

class ApplicationsController extends Controller
{

    public function __construct()
    {
        // Accessible only to admins
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {
        if (Auth::user()->is_admin==1){
            $candidature = Application::paginate(20);


            return view('applications.candidature',compact('candidature'));
        } else {
            $application = Application::where('user_id',Auth::user()->id)->first();
            if ($application && $application->user_id==Auth::user()->id){

                return view('situation',compact('application'));
            } else {
                return redirect()->route('apply');
            }

        }

    }



    /**
     * @param Application $application
     */
    public function accept($id, Request $request)
    {
        //dd($request);
        if ($request->accept=='1'){
            $app = Application::where('user_id',$id)->first();
            $app->approved = 'Y';
            $app->save();
            $message = "La candidatura di ".$app->first_name.' '.$app->last_name. ' è stata approvata';
        } elseif ($request->accept=='0'){
            $app = Application::where('user_id',$id)->first();
            $app->approved = 'N';
            $app->save();
            $message = "La candidatura di ".$app->first_name.' '.$app->last_name. ' è stata rifiutata';
        }
        Mail::to($app->email)->send(new MailApproveApplication($app));

        return response()->json(['message'=>$message,'application'=>$app]);
    }

    public function store(Request $request, Application $application) {
        //dd($request);
        $userexists = Application::where('user_id',Auth::user()->id)->first();
        if ($userexists){

            $application=$userexists;
            $newcand = false;
        } else {

            $application = $application->create($request->all());
            if ($application){
                $admins = User::where('is_admin',1)->get();
                $candidate = User::find(Auth::user()->id);
                foreach ($admins as $admin){
                    Mail::to($admin->email)->send(new MailNewCandidate($candidate));
                }

            }
            $newcand = true;
        }

        return view('applications.complete',compact('application','newcand'));
    }

    public function show($id){

        $application = Application::find($id);
        if ($application && Auth::user()->is_admin==1){
            return view('situation',compact('application'));
        } else {
            return redirect()->route('home');
        }

    }



}
