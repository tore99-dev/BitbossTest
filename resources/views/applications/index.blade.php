@extends('layouts.app')

@section('content')
    <section class="px-3 py-3 pt-md-5 pb-md-4 mx-auto text-center">
        <div class="container">
            <h1>Candidature ricevute</h1>
        </div>
    </section>
    <section class="px-3 py-3 pt-md-5 pb-md-4 mx-auto text-center">

            <div>
                <table>
                    @foreach($candidature as $cand)
                    <tr>
                        <td>{{ $cand->first_name }}</td>
                        <td>{{ $cand->last_name }}</td>
                        <td><a href="" id="approve_{{ $cand->id }}">{{__("Approve")}}</a></td>
                        <td><a href="" onclick="" id="refuse_{{ $cand->id }}">{{__("Refuse")}}</a></td>
                    </tr>
                        <script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
                    <script>
                        $(document).ready(function() {

                        });
                    </script>
                    @endforeach

                </table>


                <div>{{ $candidature->links() }}</div>
            </div>

    </section>

@endsection