@extends('layouts.app')

@section('content')
    <section class="">
        <div class="container">
            <div class="section-content">

                @if($newcand==true)
                    <h1 class="section-header">Grazie per la tua candidatura  <span class="content-header wow fadeIn " data-wow-delay="0.2s" data-wow-duration="2s"> {{$application->first_name}}</span></h1>
                    <h3>Riceverai presto una mail di risposta</h3>
                @else
                    <h2 class="section-header">Gentile <span class="content-header wow fadeIn " data-wow-delay="0.2s" data-wow-duration="2s"> {{$application->first_name}}</span>, la tua candidatura è già presente nei nostri archivi. <br><br>Visualizza <a href='{{route('applications.show',$application->id)}}'>qui</a> la tua candidatura.</h2>
                @endif

            </div>

            <div class="contact-section">
                <div class="container">
                    <a class="btn btn-default home-button submit" href="{{route('home')}}">Torna alla home</a>
                </div>
            </div>
        </div>
    </section>
@stop
