@extends('layouts.app')

@section('content')
    <section class="px-3 py-3 pt-md-5 pb-md-4 mx-auto text-center">
        <div class="container">
            <h1>Candidature ricevute</h1>
        </div>
    </section>
    <section class="px-3 py-3 pt-md-5 pb-md-4 mx-auto text-center">

        <div>
            <div id="message" style="height: 75px"></div>

            <table>
                @foreach($candidature as $cand)
                    <tr>
                        <td>{{ $cand->first_name }}</td>
                        <td>{{ $cand->last_name }}</td>
                        <td id="tdapprove_{{ $cand->id }}"><button id="approve_{{ $cand->id }}" class="btn btn{{ $cand->approved=='Y' ? '' : '-outline' }}-success btn-sm" {{ $cand->approved=='Y' ? 'disabled' : '' }}>{{ $cand->approved=='Y' ? __("Approved") : __("Approve") }}</button></td>
                        <td id="tdrefuse_{{ $cand->id }}"><button id="refuse_{{ $cand->id }}" class="btn btn{{ $cand->approved=='N' ? '' : '-outline' }}-danger btn-sm" {{ $cand->approved=='N' ? 'disabled' : '' }}>{{ $cand->approved=='N' ? __("Refused") : __("Refuse") }}</button></td>
                        <td><a href="{{route('applications.show',$cand->id)}}">View</a></td>
                    </tr>
                    <script  type="text/javascript">
                        $(document).ready(function() {
                            $('#approve_{{ $cand->id }}').click(function () {

                                $.ajax({
                                    url: '{{url('accept/'.$cand->user_id)}}/?accept=1',
                                    type: 'get',

                                    success: function (data) {
                                        var message = `<div class="alert alert-info" role="alert">
                                                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                            <strong>${data.message}</strong>
                                                        </div>`;
                                        $('#message').html(message);

                                        window.setTimeout(function() {
                                            $(".alert").fadeTo(500, 0).slideUp(500, function(){
                                                $(this).remove();
                                            });
                                        }, 2000);
                                        $('#approve_{{ $cand->id }}').removeClass('btn-outline-success btn-sm');
                                        $('#approve_{{ $cand->id }}').addClass('btn btn-success btn-sm');
                                        $('#approve_{{ $cand->id }}').attr('disabled','disabled');
                                        $('#approve_{{ $cand->id }}').html('Approved');

                                        $('#refuse_{{ $cand->id }}').removeClass('btn-danger btn-sm');
                                        $('#refuse_{{ $cand->id }}').addClass('btn btn-outline-danger btn-sm');
                                        $('#refuse_{{ $cand->id }}').removeAttr('disabled');
                                        $('#refuse_{{ $cand->id }}').html('Refuse');
                                    }
                                });
                            });

                            $('#refuse_{{ $cand->id }}').click(function () {

                                $.ajax({
                                    url: '{{url('accept/'.$cand->user_id)}}/?accept=0',
                                    type: 'get',

                                    success: function (data) {
                                        var message = `<div class="alert alert-info" role="alert">
                                                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                            <strong>${data.message}</strong>
                                                        </div>`;
                                        $('#message').html(message);
                                        window.setTimeout(function() {
                                            $(".alert").fadeTo(500, 0).slideUp(500, function(){
                                                $(this).remove();
                                            });
                                        }, 2000);
                                        $('#refuse_{{ $cand->id }}').removeClass('btn-outline-danger btn-sm');
                                        $('#refuse_{{ $cand->id }}').addClass('btn btn-danger btn-sm');
                                        $('#refuse_{{ $cand->id }}').attr('disabled','disabled');
                                        $('#refuse_{{ $cand->id }}').html('Refused');

                                        $('#approve_{{ $cand->id }}').removeClass('btn-success btn-sm');
                                        $('#approve_{{ $cand->id }}').addClass('btn btn-outline-success btn-sm');
                                        $('#approve_{{ $cand->id }}').removeAttr('disabled');
                                        $('#approve_{{ $cand->id }}').html('Approve');
                                    }
                                });
                            });
                        });
                    </script>
                @endforeach

            </table>


            <div>{{ $candidature->links() }}</div>
        </div>

    </section>

<script>

</script>
@endsection