@extends('layouts.app')

@section('content')
    <section class="px-3 py-3 pt-md-5 pb-md-4 mx-auto text-center">
        <div class="container">
            <h1>Candidatura {{Auth::user()->is_admin==1 ? $application->first_name.' '.$application->last_name : ''}}</h1>
            <h5>- inviata il giorno: {{Carbon\Carbon::parse($application->created_at)->format('d/m/Y')}} -</h5>
            <h5>- email: {{$application->email}} -</h5>
            <h3>Note:<br>{{$application->notes}}</h3>
        </div>
    </section>
    <section class="px-3 py-3 pt-md-5 pb-md-4 mx-auto text-center">
        La candidatura è stata
        @switch($application->approved)
            @case('P')
                In corso di analisi
            @break
            @case('Y')
                <b>Accettata</b> in data {{Carbon\Carbon::parse($application->updated_at)->format('d/m/Y')}}
            @break
            @case('N')
                <b>Rifiutata</b> in data {{Carbon\Carbon::parse($application->updated_at)->format('d/m/Y')}}
            @break
        @endswitch{{--Refused / Accepted at date ...--}}
    </section>
@stop
