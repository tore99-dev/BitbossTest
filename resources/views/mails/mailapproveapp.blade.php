@component('mail::message')
{{ config('app.name') }}

Salve {{$application->first_name}} {{$application->last_name}},
la tua candidatura è stata {{$application->approved == 'Y' ? 'approvata' : 'rifiutata'}}

@component('mail::button', ['url' => env('APP_URL') ])
Accedi
@endcomponent
per visualizzare la tua candidatura.

Cordiali saluti,<br>
{{ config('app.name') }}
@endcomponent
