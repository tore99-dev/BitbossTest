@component('mail::message')
{{ config('app.name') }}

Salve admin,
{{ $candidato->first_name }} {{ $candidato->last_name }} ha appena inviato la candidatura.

@component('mail::button', ['url' => env('APP_URL') ])
Accedi
@endcomponent
per visualizzare le candidature.

Cordiali saluti,<br>
{{ config('app.name') }}
@endcomponent
