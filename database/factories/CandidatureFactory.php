<?php

use Faker\Generator as Faker;
use App\Models\Application;

/*$factory->define(Application::class, function (Faker $faker) {
    return [
        //
    ];
});*/
$factory->define(Application::class, function (Faker $faker) {
    return [
        'first_name' => $faker->firstName,
        'last_name' => $faker->lastName,
        'email' => $faker->email,
        'phone' => $faker->PhoneNumber,
        'notes' => $faker->text(100),
        'approved' => 'P',
    ];
});
