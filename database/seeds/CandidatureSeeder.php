<?php

use Illuminate\Database\Seeder;
use App\Models\Application;

class CandidatureSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $admin = new User();
        $admin->name = 'Admin';
        $admin->email = 'salvatore.michalette@gmail.com';
        $admin->password = bcrypt('pass_tore123');
        $admin->is_admin = 1;
        $admin->save();

        factory(App\Models\User::class,30)->create()
            ->each(function ($user) {
                $user->application()->save(factory(Application::class)->make());
            }
        );

    }
}
