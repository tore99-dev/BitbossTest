<?php

use Illuminate\Database\Seeder;


class ApplicationsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $admin = new \App\Models\User();
        $admin->name = 'Admin';
        $admin->email = 'salvatore.michalette@varcode.it';
        $admin->password = bcrypt('pass_tore123');
        $admin->is_admin = 1;
        $admin->save();

        factory(\App\Models\User::class,30)->create()
            ->each(function ($user) {
                $user->candidatura()->save(factory(App\Models\Application::class)->make());
            }
            );

    }
}
